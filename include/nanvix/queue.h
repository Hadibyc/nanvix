
#ifndef _QUEUE_H_
#define _QUEUE_H_
#include <nanvix/pm.h>
struct file_proc
{
	int head;
	int queue;
	struct process *Tab[PROC_MAX];
	
};
void init_queue(struct file_proc* f);

int file_vide(struct file_proc* f);

int file_pleine(struct file_proc* f);

struct process* defiler(struct file_proc* f);

int enfiler(struct file_proc* f,struct process *p);

int contain(struct file_proc* f,struct process* p);

#endif