00000000000i[      ] Bochs x86 Emulator 2.6.9
00000000000i[      ]   Built from SVN snapshot on April 9, 2017
00000000000i[      ] Compiled on Jan 17 2022 at 00:25:06
00000000000i[      ] System configuration
00000000000i[      ]   processors: 1 (cores=1, HT threads=1)
00000000000i[      ]   A20 line support: yes
00000000000i[      ] IPS is set to 4000000
00000000000i[      ] CPU configuration
00000000000i[      ]   SMP support: no
00000000000i[      ]   level: 6
00000000000i[      ]   APIC support: xapic
00000000000i[      ]   FPU support: yes
00000000000i[      ]   MMX support: yes
00000000000i[      ]   3dnow! support: no
00000000000i[      ]   SEP support: yes
00000000000i[      ]   SIMD support: sse2
00000000000i[      ]   XSAVE support: no 
00000000000i[      ]   AES support: no
00000000000i[      ]   SHA support: no
00000000000i[      ]   MOVBE support: no
00000000000i[      ]   ADX support: no
00000000000i[      ]   x86-64 support: no
00000000000i[      ]   MWAIT support: yes
00000000000i[      ] Optimization configuration
00000000000i[      ]   RepeatSpeedups support: yes
00000000000i[      ]   Fast function calls: yes
00000000000i[      ]   Handlers Chaining speedups: no
00000000000i[      ] Devices configuration
00000000000i[      ]   PCI support: i440FX i430FX
00000000000i[      ]   Networking: no
00000000000i[      ]   Sound support: no
00000000000i[      ]   USB support: no
00000000000i[      ]   VGA extension support: vbe
00000000000i[MEM0  ] allocated memory at 0x7f7ef88cc010. after alignment, vector=0x7f7ef88cd000
00000000000i[MEM0  ] 16,00MB
00000000000i[MEM0  ] mem block size = 0x00020000, blocks=128
00000000000i[MEM0  ] rom at 0xfffe0000/131072 ('/usr/local/share/bochs/BIOS-bochs-latest')
00000000000i[PLUGIN] init_dev of 'pci' plugin device by virtual method
00000000000i[DEV   ] i440FX PMC present at device 0, function 0
00000000000i[PLUGIN] init_dev of 'pci2isa' plugin device by virtual method
00000000000i[DEV   ] PIIX3 PCI-to-ISA bridge present at device 1, function 0
00000000000i[PLUGIN] init_dev of 'cmos' plugin device by virtual method
00000000000i[CMOS  ] Using utc time for initial clock
00000000000i[CMOS  ] Setting initial clock to: Thu Mar 31 18:20:05 2022 (time0=1648743605)
00000000000i[PLUGIN] init_dev of 'dma' plugin device by virtual method
00000000000i[DMA   ] channel 4 used by cascade
00000000000i[PLUGIN] init_dev of 'pic' plugin device by virtual method
00000000000i[PLUGIN] init_dev of 'pit' plugin device by virtual method
00000000000i[PLUGIN] init_dev of 'vga' plugin device by virtual method
00000000000i[MEM0  ] Register memory access handlers: 0x0000000a0000 - 0x0000000bffff
00000000000i[VGA   ] interval=200000, mode=realtime
00000000000i[MEM0  ] Register memory access handlers: 0x0000e0000000 - 0x0000e0ffffff
00000000000i[BXVGA ] VBE Bochs Display Extension Enabled
00000000000i[MEM0  ] rom at 0xc0000/41472 ('/usr/local/share/bochs/VGABIOS-lgpl-latest')
00000000000i[PLUGIN] init_dev of 'floppy' plugin device by virtual method
00000000000i[DMA   ] channel 2 used by Floppy Drive
00000000000i[FLOPPY] Using boot sequence cdrom, none, none
00000000000i[FLOPPY] Floppy boot signature check is enabled
00000000000i[PLUGIN] init_dev of 'acpi' plugin device by virtual method
00000000000i[DEV   ] ACPI Controller present at device 1, function 3
00000000000i[PLUGIN] init_dev of 'ioapic' plugin device by virtual method
00000000000i[IOAPIC] initializing I/O APIC
00000000000i[MEM0  ] Register memory access handlers: 0x0000fec00000 - 0x0000fec00fff
00000000000i[IOAPIC] IOAPIC enabled (base address = 0xfec00000)
00000000000i[PLUGIN] init_dev of 'keyboard' plugin device by virtual method
00000000000i[KBD   ] will paste characters every 400 keyboard ticks
00000000000i[PLUGIN] init_dev of 'harddrv' plugin device by virtual method
00000000000i[HD    ] HD on ata0-0: 'hdd.img', 'flat' mode
00000000000i[IMG   ] hd_size: 67108864
00000000000i[HD    ] ata0-0: using specified geometry: CHS=130/16/63
00000000000i[HD    ] ata0-0: extra data outside of CHS address range
00000000000i[HD    ] CD on ata0-1: 'nanvix.iso'
00000000000i[CD1   ] load cdrom with path='nanvix.iso'
00000000000i[CD1   ] Opening image file as a cd.
00000000000i[HD    ] Media present in CD-ROM drive
00000000000i[HD    ] Capacity is 544 sectors (1,06 MB)
00000000000i[HD    ] translation on ata0-0 set to 'none'
00000000000i[PLUGIN] init_dev of 'pci_ide' plugin device by virtual method
00000000000i[DEV   ] PIIX3 PCI IDE controller present at device 1, function 1
00000000000i[PLUGIN] init_dev of 'unmapped' plugin device by virtual method
00000000000i[PLUGIN] init_dev of 'biosdev' plugin device by virtual method
00000000000i[PLUGIN] init_dev of 'speaker' plugin device by virtual method
00000000000e[PCSPK ] Failed to open /dev/console: Permission non accordée
00000000000e[PCSPK ] Deactivating beep on console
00000000000i[PLUGIN] init_dev of 'extfpuirq' plugin device by virtual method
00000000000i[PLUGIN] init_dev of 'parallel' plugin device by virtual method
00000000000i[PAR   ] parallel port 1 at 0x0378 irq 7
00000000000i[PLUGIN] init_dev of 'serial' plugin device by virtual method
00000000000i[SER   ] com1 at 0x03f8 irq 4 (mode: null)
00000000000i[PLUGIN] register state of 'pci' plugin device by virtual method
00000000000i[PLUGIN] register state of 'pci2isa' plugin device by virtual method
00000000000i[PLUGIN] register state of 'cmos' plugin device by virtual method
00000000000i[PLUGIN] register state of 'dma' plugin device by virtual method
00000000000i[PLUGIN] register state of 'pic' plugin device by virtual method
00000000000i[PLUGIN] register state of 'pit' plugin device by virtual method
00000000000i[PLUGIN] register state of 'vga' plugin device by virtual method
00000000000i[PLUGIN] register state of 'floppy' plugin device by virtual method
00000000000i[PLUGIN] register state of 'unmapped' plugin device by virtual method
00000000000i[PLUGIN] register state of 'biosdev' plugin device by virtual method
00000000000i[PLUGIN] register state of 'speaker' plugin device by virtual method
00000000000i[PLUGIN] register state of 'extfpuirq' plugin device by virtual method
00000000000i[PLUGIN] register state of 'parallel' plugin device by virtual method
00000000000i[PLUGIN] register state of 'serial' plugin device by virtual method
00000000000i[PLUGIN] register state of 'acpi' plugin device by virtual method
00000000000i[PLUGIN] register state of 'ioapic' plugin device by virtual method
00000000000i[PLUGIN] register state of 'keyboard' plugin device by virtual method
00000000000i[PLUGIN] register state of 'harddrv' plugin device by virtual method
00000000000i[PLUGIN] register state of 'pci_ide' plugin device by virtual method
00000000000i[SYS   ] bx_pc_system_c::Reset(HARDWARE) called
00000000000i[CPU0  ] cpu hardware reset
00000000000i[APIC0 ] allocate APIC id=0 (MMIO enabled) to 0x0000fee00000
00000000000i[CPU0  ] CPUID[0x00000000]: 00000005 756e6547 6c65746e 49656e69
00000000000i[CPU0  ] CPUID[0x00000001]: 00000633 00010800 00000008 1fcbfbff
00000000000i[CPU0  ] CPUID[0x00000002]: 00410601 00000000 00000000 00000000
00000000000i[CPU0  ] CPUID[0x00000003]: 00000000 00000000 00000000 00000000
00000000000i[CPU0  ] CPUID[0x00000004]: 00000000 00000000 00000000 00000000
00000000000i[CPU0  ] CPUID[0x00000005]: 00000040 00000040 00000003 00000020
00000000000i[CPU0  ] CPUID[0x80000000]: 80000008 00000000 00000000 00000000
00000000000i[CPU0  ] CPUID[0x80000001]: 00000000 00000000 00000000 00000000
00000000000i[CPU0  ] CPUID[0x80000002]: 20202020 20202020 20202020 6e492020
00000000000i[CPU0  ] CPUID[0x80000003]: 286c6574 50202952 69746e65 52286d75
00000000000i[CPU0  ] CPUID[0x80000004]: 20342029 20555043 20202020 00202020
00000000000i[CPU0  ] CPUID[0x80000005]: 01ff01ff 01ff01ff 40020140 40020140
00000000000i[CPU0  ] CPUID[0x80000006]: 00000000 42004200 02008140 00000000
00000000000i[CPU0  ] CPUID[0x80000007]: 00000000 00000000 00000000 00000000
00000000000i[CPU0  ] CPUID[0x80000008]: 00002028 00000000 00000000 00000000
00000000000i[PLUGIN] reset of 'pci' plugin device by virtual method
00000000000i[PLUGIN] reset of 'pci2isa' plugin device by virtual method
00000000000i[PLUGIN] reset of 'cmos' plugin device by virtual method
00000000000i[PLUGIN] reset of 'dma' plugin device by virtual method
00000000000i[PLUGIN] reset of 'pic' plugin device by virtual method
00000000000i[PLUGIN] reset of 'pit' plugin device by virtual method
00000000000i[PLUGIN] reset of 'vga' plugin device by virtual method
00000000000i[PLUGIN] reset of 'floppy' plugin device by virtual method
00000000000i[PLUGIN] reset of 'acpi' plugin device by virtual method
00000000000i[PLUGIN] reset of 'ioapic' plugin device by virtual method
00000000000i[PLUGIN] reset of 'keyboard' plugin device by virtual method
00000000000i[PLUGIN] reset of 'harddrv' plugin device by virtual method
00000000000i[PLUGIN] reset of 'pci_ide' plugin device by virtual method
00000000000i[PLUGIN] reset of 'unmapped' plugin device by virtual method
00000000000i[PLUGIN] reset of 'biosdev' plugin device by virtual method
00000000000i[PLUGIN] reset of 'speaker' plugin device by virtual method
00000000000i[PLUGIN] reset of 'extfpuirq' plugin device by virtual method
00000000000i[PLUGIN] reset of 'parallel' plugin device by virtual method
00000000000i[PLUGIN] reset of 'serial' plugin device by virtual method
00000004662i[BIOS  ] $Revision: 13073 $ $Date: 2017-02-16 22:43:52 +0100 (Do, 16. Feb 2017) $
00000318050i[KBD   ] reset-disable command received
00000320819i[BIOS  ] Starting rombios32
00000321257i[BIOS  ] Shutdown flag 0
00000321841i[BIOS  ] ram_size=0x01000000
00000322262i[BIOS  ] ram_end=16MB
00000362830i[BIOS  ] Found 1 cpu(s)
00000376414i[BIOS  ] bios_table_addr: 0x000f9cd8 end=0x000fcc00
00000704209i[PCI   ] i440FX PMC write to PAM register 59 (TLB Flush)
00001032138i[P2ISA ] PCI IRQ routing: PIRQA# set to 0x0b
00001032157i[P2ISA ] PCI IRQ routing: PIRQB# set to 0x09
00001032176i[P2ISA ] PCI IRQ routing: PIRQC# set to 0x0b
00001032195i[P2ISA ] PCI IRQ routing: PIRQD# set to 0x09
00001032205i[P2ISA ] write: ELCR2 = 0x0a
00001032975i[BIOS  ] PIIX3/PIIX4 init: elcr=00 0a
00001040656i[BIOS  ] PCI: bus=0 devfn=0x00: vendor_id=0x8086 device_id=0x1237 class=0x0600
00001042935i[BIOS  ] PCI: bus=0 devfn=0x08: vendor_id=0x8086 device_id=0x7000 class=0x0601
00001045053i[BIOS  ] PCI: bus=0 devfn=0x09: vendor_id=0x8086 device_id=0x7010 class=0x0101
00001045282i[PIDE  ] new BM-DMA address: 0xc000
00001045898i[BIOS  ] region 4: 0x0000c000
00001047935i[BIOS  ] PCI: bus=0 devfn=0x0b: vendor_id=0x8086 device_id=0x7113 class=0x0680
00001048167i[ACPI  ] new irq line = 11
00001048179i[ACPI  ] new irq line = 9
00001048204i[ACPI  ] new PM base address: 0xb000
00001048218i[ACPI  ] new SM base address: 0xb100
00001048246i[PCI   ] setting SMRAM control register to 0x4a
00001212339i[CPU0  ] Enter to System Management Mode
00001212350i[CPU0  ] RSM: Resuming from System Management Mode
00001376371i[PCI   ] setting SMRAM control register to 0x0a
00001391237i[BIOS  ] MP table addr=0x000f9db0 MPC table addr=0x000f9ce0 size=0xc8
00001393059i[BIOS  ] SMBIOS table addr=0x000f9dc0
00001395227i[BIOS  ] ACPI tables: RSDP addr=0x000f9ee0 ACPI DATA addr=0x00ff0000 size=0xf72
00001398417i[BIOS  ] Firmware waking vector 0xff00cc
00001400212i[PCI   ] i440FX PMC write to PAM register 59 (TLB Flush)
00001400935i[BIOS  ] bios_table_cur_addr: 0x000f9f04
00001528552i[VBIOS ] VGABios $Id: vgabios.c,v 1.76 2013/02/10 08:07:03 vruppert Exp $
00001528623i[BXVGA ] VBE known Display Interface b0c0
00001528655i[BXVGA ] VBE known Display Interface b0c5
00001531580i[VBIOS ] VBE Bios $Id: vbe.c,v 1.65 2014/07/08 18:02:25 vruppert Exp $
00001876023i[BIOS  ] ata0-0: PCHS=130/16/63 translation=none LCHS=130/16/63
00014424477i[BIOS  ] Booting from 07c0:0000
00014530800i[BIOS  ] int13_harddisk: function 41, unmapped device for ELDL=81
00014534477i[BIOS  ] int13_harddisk: function 08, unmapped device for ELDL=81
00014538138i[BIOS  ] *** int 15h function AX=00c0, BX=0000 not yet supported!
00020701925i[HD    ] disk ata1-0 not present, aborting
00131393115i[TERM  ] ips = 131,393M
00263183693i[TERM  ] ips = 131,791M
00393109000i[TERM  ] ips = 129,925M
00514225164i[TERM  ] ips = 121,116M
00635915264i[TERM  ] ips = 121,690M
00765911406i[TERM  ] ips = 129,996M
00898105200i[TERM  ] ips = 132,194M
01024975001i[TERM  ] ips = 126,870M
01154380637i[TERM  ] ips = 129,406M
01279758220i[TERM  ] ips = 125,378M
01411031701i[TERM  ] ips = 131,273M
01538947310i[TERM  ] ips = 127,916M
01658702479i[TERM  ] ips = 119,755M
01784940903i[TERM  ] ips = 126,238M
01919807555i[TERM  ] ips = 134,867M
02037936974i[TERM  ] ips = 118,129M
02166005637i[TERM  ] ips = 128,069M
02298241641i[TERM  ] ips = 132,236M
02429639048i[TERM  ] ips = 131,397M
02549917855i[TERM  ] ips = 120,279M
02673911128i[TERM  ] ips = 123,993M
02799128494i[TERM  ] ips = 125,217M
02929050362i[TERM  ] ips = 129,922M
03062458351i[TERM  ] ips = 133,408M
03192413976i[TERM  ] ips = 129,956M
03320386098i[TERM  ] ips = 127,972M
03446144973i[TERM  ] ips = 125,759M
03581757283i[TERM  ] ips = 135,612M
03711647640i[TERM  ] ips = 129,890M
03838482120i[TERM  ] ips = 126,834M
03968338241i[TERM  ] ips = 129,856M
04065622137i[TERM  ] ips = 97,284M
04191765966i[TERM  ] ips = 126,144M
04328256708i[TERM  ] ips = 136,491M
04464285843i[TERM  ] ips = 136,029M
04597410529i[TERM  ] ips = 133,125M
04731984265i[TERM  ] ips = 134,574M
04866356553i[TERM  ] ips = 134,372M
04998333916i[TERM  ] ips = 131,977M
05133308131i[TERM  ] ips = 134,974M
05259023548i[TERM  ] ips = 125,715M
05393721286i[TERM  ] ips = 134,698M
05525481004i[TERM  ] ips = 131,760M
05650072756i[TERM  ] ips = 124,592M
05783716910i[TERM  ] ips = 133,644M
05916433786i[TERM  ] ips = 132,717M
06050663238i[TERM  ] ips = 134,229M
06183927841i[TERM  ] ips = 133,265M
06316180217i[TERM  ] ips = 132,252M
06450140829i[TERM  ] ips = 133,961M
06578738760i[TERM  ] ips = 128,598M
06713816320i[TERM  ] ips = 135,078M
06851656432i[TERM  ] ips = 137,840M
06988614471i[TERM  ] ips = 136,958M
07125394139i[TERM  ] ips = 136,780M
07258028000i[TERM  ] ips = 132,634M
07394298691i[TERM  ] ips = 136,271M
07530010888i[TERM  ] ips = 135,712M
07661757313i[TERM  ] ips = 131,746M
07798629776i[TERM  ] ips = 136,872M
07927956099i[TERM  ] ips = 129,326M
08061866581i[TERM  ] ips = 133,910M
08196597148i[TERM  ] ips = 134,731M
08327989998i[TERM  ] ips = 131,393M
08461910547i[TERM  ] ips = 133,921M
08593013566i[TERM  ] ips = 131,103M
08720159927i[TERM  ] ips = 127,146M
08849577087i[TERM  ] ips = 129,417M
08979404630i[TERM  ] ips = 129,828M
09093621433i[TERM  ] ips = 114,217M
09208966116i[TERM  ] ips = 115,345M
09327179529i[TERM  ] ips = 118,213M
09453514160i[TERM  ] ips = 126,335M
09587530029i[TERM  ] ips = 134,016M
09712216000i[TERM  ] ips = 124,686M
09796248807i[TERM  ] ips = 84,033M
09920982441i[TERM  ] ips = 124,734M
10047730433i[TERM  ] ips = 126,748M
10178319013i[TERM  ] ips = 130,589M
10302318438i[TERM  ] ips = 123,999M
10402193219i[TERM  ] ips = 99,875M
10514094430i[TERM  ] ips = 111,901M
10562327398i[TERM  ] ips = 48,233M
10656270667i[TERM  ] ips = 93,943M
10784347838i[TERM  ] ips = 128,077M
10906320474i[TERM  ] ips = 121,973M
11025424290i[TERM  ] ips = 119,104M
11150061667i[TERM  ] ips = 124,637M
11270115271i[TERM  ] ips = 120,054M
11362290965i[TERM  ] ips = 92,176M
11446866386i[TERM  ] ips = 84,575M
11499600019i[TERM  ] ips = 52,734M
11567331632i[TERM  ] ips = 67,732M
11649527743i[TERM  ] ips = 82,196M
11713293129i[TERM  ] ips = 63,765M
11791733475i[TERM  ] ips = 78,440M
11909556380i[TERM  ] ips = 117,823M
12030792577i[TERM  ] ips = 121,236M
12167617002i[TERM  ] ips = 136,824M
12301628892i[TERM  ] ips = 134,012M
12437913447i[TERM  ] ips = 136,285M
12553411144i[TERM  ] ips = 115,498M
12672927105i[TERM  ] ips = 119,516M
12792024616i[TERM  ] ips = 119,098M
12920405707i[TERM  ] ips = 128,381M
13047154431i[TERM  ] ips = 126,749M
13176207211i[TERM  ] ips = 129,053M
13299636000i[TERM  ] ips = 123,429M
13421252186i[TERM  ] ips = 121,616M
13529142964i[TERM  ] ips = 107,891M
13649228233i[TERM  ] ips = 120,085M
13763229401i[TERM  ] ips = 114,001M
13890036000i[TERM  ] ips = 126,807M
14021897821i[TERM  ] ips = 131,862M
14153324963i[TERM  ] ips = 131,427M
14284851606i[TERM  ] ips = 131,527M
14418137497i[TERM  ] ips = 133,286M
14545951162i[TERM  ] ips = 127,814M
14676714863i[TERM  ] ips = 130,764M
14805395195i[TERM  ] ips = 128,680M
14934878603i[TERM  ] ips = 129,483M
15065837770i[TERM  ] ips = 130,959M
15194128000i[TERM  ] ips = 128,290M
15327048870i[TERM  ] ips = 132,921M
15464289497i[TERM  ] ips = 137,241M
15601893870i[TERM  ] ips = 137,604M
15739515362i[TERM  ] ips = 137,621M
15870657227i[TERM  ] ips = 131,142M
16003023518i[TERM  ] ips = 132,366M
16136822621i[TERM  ] ips = 133,799M
16261114813i[TERM  ] ips = 124,292M
16393064842i[TERM  ] ips = 131,950M
16521127894i[TERM  ] ips = 128,063M
16647064487i[TERM  ] ips = 125,937M
16773973387i[TERM  ] ips = 126,909M
16908329000i[TERM  ] ips = 134,356M
17039258969i[TERM  ] ips = 130,930M
17169484375i[TERM  ] ips = 130,225M
17298436201i[TERM  ] ips = 128,952M
17425712516i[TERM  ] ips = 127,276M
17551407282i[TERM  ] ips = 125,695M
17675792780i[TERM  ] ips = 124,385M
17800391389i[TERM  ] ips = 124,599M
17929455124i[TERM  ] ips = 129,064M
18057571806i[TERM  ] ips = 128,117M
18173504416i[TERM  ] ips = 115,933M
18302020016i[TERM  ] ips = 128,516M
18427224000i[TERM  ] ips = 125,204M
18559254406i[TERM  ] ips = 132,030M
18688999261i[TERM  ] ips = 129,745M
18816346706i[TERM  ] ips = 127,347M
18947381397i[TERM  ] ips = 131,035M
