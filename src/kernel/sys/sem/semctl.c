#include <nanvix/sem.h>
#include <nanvix/syscall.h>
#include <nanvix/klib.h>



PUBLIC int sys_semctl(int key, int cmd, int val)
{
    int index_semaphore = contains_Sem_tab(key); 

    if (index_semaphore < 0 || val < 0)
    {
        return -1;
    }
    
    switch (cmd)
    {
    // GETVAL
    case 0:
        return Sem_tab[index_semaphore].nb_resources;
        break;
    
    // SETVAL
    case 1:
        Sem_tab[index_semaphore].nb_resources = val;
        return 0;
        break;
        
    // IPC_RMID    
    case 3:
        if(Sem_tab[index_semaphore].nb_resources >= 0)
        {
            destroy(&Sem_tab[index_semaphore]);
            return 0;
        }else{
            return -1;
        }
        break;  
        
    default:
        return -1;
        break;
    }
}

