#include <nanvix/pm.h>
#include <nanvix/queue.h>
#include <nanvix/sem.h>
#include <nanvix/klib.h>
#include <nanvix/const.h>

/* 
    returns the index of the semaphore
    which has a key=k is in Sem_tab,
    -1 otherwise.
*/
int contains_Sem_tab (int k)
{
    int i = 0;
    int notFound = 1;
    int res = -1;
    
    while(i < MAX_SEM_TAB && notFound)
    {
        if (Sem_tab[i].key == k &&  Sem_tab[i].valid==1 )
        {
            res = i;
            notFound = 0;
        }
        i++;
    }
    
    return res;
}


/*
    returns the index of the first invalid semaphore in Sem_tab,
    -1 if there is no such semaphore
*/
int insert_Sem_tab()
{
    int res = -1;
    int found = 0;
    int  i = 0; 

    while(i < MAX_SEM_TAB && !found)
    {
        if (Sem_tab[i].valid == 0)
        {
            res = i;
            found = 1;
        }
        i++;
    }

    return res;
}

/*  
creating the semaphore with key=k and nb_ressources=n
returns the index of the semaphore in Sem_tab 
and -1 in case of errors
*/
int create (int k, int n)
{
    int index = insert_Sem_tab();

    if (index >= 0)
    {   
        Sem_tab[index].valid = 1; 
        Sem_tab[index].key = k;
        Sem_tab[index].nb_resources = n;
    }
    
    return index;
}

// updates the attribute valid {0,1}
void destroy (struct semaphore* s)
{
    int existing_sem = contains_Sem_tab(s->key);
    
    if (existing_sem > 0)
    {
        Sem_tab[existing_sem].valid = 0;
    }
}

// equivalent to acquire()
void down (struct semaphore* s)
{
    disable_interrupts(); 

    s->nb_resources--;

    if (s->nb_resources < 0)
    {
        sleep(&s->wait_list, curr_proc->priority);
    }

    enable_interrupts();
}

// equivalent to release()
void up (struct semaphore* s)
{
    disable_interrupts();
    
    s->nb_resources++; 
    
    if (s->nb_resources <= 0)
    {
        wakeup_first_process(&(s->wait_list));
    }
    
    enable_interrupts(); 
}

/* 
    function made for initialization of sem_tab 
    at the beginning of the execution , called in init/main.c.
 */
void sem_init(){

    for (int i = 0; i < MAX_SEM_TAB; i++)
    {
        struct semaphore s;
        s.wait_list = NULL;
        s.nb_resources = 0;
        s.valid = 0;
        s.key = -1;
        Sem_tab[i] = s;
    } 
}




	