#include <nanvix/queue.h>

void init_queue(struct file_proc* f){
	f->head = 0;
	f->queue = 0;
}

int file_vide(struct file_proc* f)
{
	if (f->head == f->queue)
		return 1;
	else
		return 0;
}

int file_pleine(struct file_proc* f)
{
	if (f->head < f->queue)
	{
		if ((f->queue - f->head) == (PROC_MAX - 1))
		{
			return 1;
		}
		else
			return 0;
	}
	else
	{
		if (f->head > f->queue)
		{
			if (f->head - f->queue == 1)
			{
				return 1;
			}
			else
				return 0;
		}
	}
	return 0;
}

struct process* defiler(struct file_proc* f)
{
	if (file_vide(f))
		return NULL;
	struct process * result = f->Tab[f->head];
	f->Tab[f->head] = NULL;
	int m;
	m = f->head + 1;
	f->head = m % PROC_MAX;
	return result;
}

int enfiler(struct file_proc* f,struct process *p)
{
	int m;
	if (file_pleine(f))
		return 0;
	f->Tab[f->queue] = p;
	m = f->queue + 1;
	f->queue = m % PROC_MAX;

	return 1;
}

// Il fallait pas parcourir toute la file, il suffit de partir de 0 jusqu'a la queue seulement
// Il fallait aussi ajouter la condition suivante:  if (!IS_VALID(p)) continue;
int contain (struct file_proc* f, struct process* p){
	int res = 0;
	
	
	if (file_vide(f))
	{
		return 0;
	}
	
	if (f->head < f->queue)
	{
		for (int i = f->head; i < f->queue; i++)
		{
			if ((f->Tab[i] == p) && (IS_VALID(p)))
			{
				res = 1;
				break;
			}
		}
	}else
	{

		for (int i = f->head; i < PROC_MAX; i++)
		{
			if ((f->Tab[i] == p) && (IS_VALID(p)))
			{
				res = 1;
				break;
			}
		}
		for (int i = 0; i < f->queue; i++)
		{
			if ((f->Tab[i] == p) && (IS_VALID(p)))
			{
				res = 1;
				break;
			}
		}
	}
	
	return res;
}